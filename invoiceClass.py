
from asyncio.windows_events import NULL
import unittest

class Invoice():

    def __init__(self, num = NULL, description = NULL, quantity = NULL, price = NULL):
        self.num = num
        self.description = description
        self.quantity = quantity
        self.price = price

        if(quantity > 0 ): self.quantity = quantity
        else: self.quantity = 0

        if(price >= 0.00): self.price = price 
        else: self.price = 0.00

    
    @property
    def num(self):
        return self._num

    @num.setter
    def num(self, value):
        self._num = value

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

    @property
    def quantity(self):
        return self._quantity

    @quantity.setter
    def quantity(self, value):
        if(value > 0 ): self._quantity = value
        else: self._quantity = 0

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self,value):
        if(value >= 0.00): self._price = value 
        else: self._price = 0.00

    def getInvoiceAmount(self, quantity, price):
        return quantity * price

    
class TestInvoice(unittest.TestCase):
    
    def testInstance(self):
        invoice5 = Invoice('5', 'switch', -10, -87.99)
        self.assertEqual(invoice5._price, 0.00)
        self.assertTrue(invoice5._quantity == 0)
    
    def testAttributes(self):
        invoice4 = Invoice('4', 'mouse', 4, 378.90)
        self.assertIsInstance(invoice4, Invoice)
        self.assertNotEqual('5', invoice4._num)
        self.assertIs('mouse', invoice4._description)
        self.assertTrue(invoice4._quantity == 4)
        self.assertEqual(378.90, invoice4._price)

    def testAmount(self):
        invoice1 = Invoice('1', 'ssd', 3, 350.83)
        self.assertEqual(1052.49,invoice1.getInvoiceAmount(invoice1.quantity, invoice1.price))

    def testNegativePrice(self):
        invoice2 = Invoice('2', 'hd', 40)
        invoice2.price = -31.23
        self.assertTrue(invoice2.getInvoiceAmount(invoice2.quantity, invoice2.price) == 0.0)

    def testZeroQuantity(self):
        invoice3 = Invoice('3', 'cooler', 0, 1211.21)
        self.assertEqual(invoice3.getInvoiceAmount(invoice3.quantity, invoice3.price), 0.0)



if __name__ == "__main__":
    unittest.main()